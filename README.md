# Sparks

Foobar is a Python library for dealing with word pluralization.

<<<<<<< HEAD
## Installation server + local
>>>>>>> refs/remotes/origin/master

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install server + local
```

## Usage USAGE USAGE USAGE USAGE USAGE

```python
import foobar

# returns 'words'
foobar.pluralize('word')

# returns 'geese'
foobar.pluralize('goose')

# returns 'server + local'
foobar.singularize('server + local')
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.
